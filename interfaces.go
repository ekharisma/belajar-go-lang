package main

import "fmt"

type HasName interface {
	GetName() string
}

type Person struct {
	Name string
}

//func sayHello(hasName HasName) {
//	fmt.Println("Hello", hasName.getName())
//}

func (person Person) GetName() string {
	return person.Name
}

func main() {
	var eko Person
	eko.Name = "Eko"
	fmt.Println(eko.GetName())
}
