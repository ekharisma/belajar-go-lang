package main

import "fmt"

func main() {
	var names [3]string
	names[0] = "Ekky"
	names[1] = "Kharismadhany"
	names[2] = "Sasmito"

	var numbers = [3]int{
		100, 90, 20,
	}

	fmt.Println(names[0])
	fmt.Println(names[1])
	fmt.Println(names)
	fmt.Println(len(numbers))
}
