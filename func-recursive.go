package main

func factorial(value int) int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i
	}
	return result
}

func recursiveFactorial(value int) int {
	if value == 1 {
		return 1
	} else {
		return value * recursiveFactorial(value - 1)
	}
}

func main() {
	println(factorial(5))
	println(recursiveFactorial(5))
}
