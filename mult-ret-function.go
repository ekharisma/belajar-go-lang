package main

func perkalianDanPembagian(a int, b int) (int, int) {
	perkalian := a * b
	pembagian := b / a
	return perkalian, pembagian
}

func main() {
	perkalian, pembagian := perkalianDanPembagian(10, 20)
	println(pembagian)
	println(perkalian)
}
