package main

import "fmt"

func endApp()  {
	message := recover()
	if message != nil {
		fmt.Println("Terjadi error ", message)
	} else {
		fmt.Println("Aplikasi selesai")
	}
}

func runAppl(error bool) {
	defer endApp()
	if error {
		panic("APLIKASI ERROR")
	} else {
		fmt.Println("Aplikasi Berjalan")
	}
}

func main() {
	runAppl(false)
}
