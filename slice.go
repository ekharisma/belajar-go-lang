package main

import "fmt"

func main() {
	var months = [...]string{
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
	}
	var slice1 = months[1:3]
	fmt.Println(cap(slice1))
	fmt.Println(len(slice1))
}
