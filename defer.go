package main

import "fmt"

func logging() {
	fmt.Println("Selesai memamnggil fungsi")
}

func runApp(value int) {
	defer logging()
	fmt.Println("Run Application")
	result := 10 / value
	fmt.Println(result)
}

func main() {
	runApp(0)
}
